window.onload = function() {
  var days = [
    'Воскресенье',
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота'
  ];
  var date = new Date();
  var day = date.getDay();

  document.querySelector('.weather__block_day').textContent = days[day];

  if (navigator.geolocation) {
    console.log('Geolocation is supported!'); 
    navigator.geolocation.getCurrentPosition(function(position){
      document.querySelector('.loader').classList.add('loader__hide');
      document.querySelector('body').classList.remove('hidden');
      document.querySelector('.weather__block').classList.add('weather__block_loaded');

      let lat = position.coords.latitude;
      let long = position.coords.longitude;

      const deg = document.querySelector('.weather__block_deg');
      const feels = document.querySelector('.weather__block_feel');
      const main = document.querySelector('.weather__block_main');
      const icon = document.querySelector('.weather__block_icon');

      const fewDeg = document.querySelectorAll('.forecast__block_degrees');
      const fewMain = document.querySelectorAll('.forecast__block_title');
      const fewIcon = document.querySelectorAll('.forecast__block_img');
      const place = document.querySelectorAll('.yourPlace');

      
      const arrDeg = [];
      const arrMain = [];
      const arrIcon = [];

      

      fetch('https://api.openweathermap.org/data/2.5/onecall?lat='+lat+'&lon='+long+'&exclude=minutely,hourly,alerts&appid=1e3ad7cd117cb679c20b58d717b99f0a')
      .then(function(resp) {
        return resp.json();
      })
      .then(function(data) {
        deg.innerHTML = Math.round(data.current.temp - 273.15) + '&deg;';
        feels.innerHTML = Math.round(data.current.feels_like - 273.15) +  '&deg;';
        main.textContent = data.current.weather[0].main;
        icon.innerHTML = '<img src="http://openweathermap.org/img/wn/'+data.current.weather[0].icon+'@2x.png">';
        place.forEach(item =>{
          item.textContent = data.timezone;
        });
        data.daily.forEach(item => {
          arrDeg.push(item.temp.day);
          arrMain.push(item.weather[0].main);
          arrIcon.push(item.weather[0].icon);
        });
        fewDeg.forEach(function(item, i){
          item.textContent = Math.round(arrDeg[i] - 273.15);
        });
        fewMain.forEach(function(item, i){
          item.textContent = arrMain[i];
        });
        fewIcon.forEach(function(item, i){
          item.innerHTML = '<img src="http://openweathermap.org/img/wn/'+arrIcon[i]+'@2x.png">';
        });
      })
      .catch(function(){
        alert('Error!');
      })
    });

  }
  else {
    alert('Включите геолокацию в браузере или на устройстве');
  }
};
  